# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
 
import json
import numpy as np

from geostack.vector import Vector
from geostack.solvers import LevelSet
from geostack.io import vectorToGeoJson

# Create initial conditions
v = Vector()
point_id = v.addPoint( [1, 1] )
v.setProperty(point_id, "radius", 10)

# Create solver
config = {
    "resolution": 1.0,
    "buildScript": "speed = 1.0;"
}
solver = LevelSet()
solver.init(json.dumps(config), v)

# Run solver
while solver.getParameters()['time'] < 100.0:
    solver.step()
      
# Write arrival time 
solver.getArrival().write('./_out_level_set_arrival.tiff')
        
# Generate isochrones
isochroneVector = solver.getArrival().vectorise(
    np.arange(0, 100, 10), 100)

# Write isochrone file
with open('./_out_level_set_iso.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(isochroneVector, enforceProjection = False))

print("Done")
