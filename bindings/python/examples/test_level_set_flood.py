import json
import numpy as np

from geostack.raster import Raster, RasterPtrList
from geostack.vector import Vector
from geostack.solvers import LevelSet
from geostack.core import Variables
from geostack.core import ProjectionParameters
from geostack.gs_enums import ReductionType

# Create raster layer
elev = Raster(name = 'elev')
elev.read('test_level_set_flood.tif')

# Create vector layer
v = Vector()
point_id = v.addPoint( [148.82465, -35.61888] )
proj_EPSG4326 = ProjectionParameters.from_proj4(
    "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
v.setProjectionParameters(proj_EPSG4326)
v.setProperty(point_id, "radius", 500)

# Create input list
inputLayers = RasterPtrList()
inputLayers.append(elev)

# Create output lists
outputLayers = RasterPtrList()
depth = Raster(name = 'depth')
extent = Raster(name = 'extent')
outputLayers.append(depth)
outputLayers.append(extent)
depth.setReductionType(ReductionType.Sum)
extent.setReductionType(ReductionType.Sum)

# Global parameters
resolution = 30
height = 1200
durationSeconds = 1800

# Create variables
variables = Variables()
variables.set("volume", 0.0)
variables.set("height", height)
      
# Create solver
solver = LevelSet()
config = {
    "projection": "+proj=utm +zone=55 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
    "resolution": resolution,
    "buildScript": "REAL c = 2.0*9.8*(height-elev)-100.0; speed = c > 0.0 ? sqrt(c) : 0.0;",
    "updateScript": "depth = max(height-elev, 0.0); extent = depth > 0.0 ? 1.0 : 0.0;",
    "timeMultiple": 600
}
solver.init(json.dumps(config), v, variables, inputLayers, outputLayers)

# Run solver
reportTime = 0
solver.step()
volumeInitial = depth.reduceVal
while solver.getParameters()['time'] <= durationSeconds:
    
    # Output time
    time = solver.getParameters()['time']
    if time >= reportTime:
        print(f'Time: {time:.0f}s')
        reportTime += 600

    # Take step
    solver.step()
    volume = depth.reduceVal
    area = extent.reduceVal
        
    # Adjust height
    dh = (volume-volumeInitial)/area
    height = height-dh
    variables.set("height", height)
        
solver.getArrival().write('./_test_level_set_flood_arrival.tif')
