# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# Correlated Brownian motion
import json
import numpy as np
import matplotlib.pyplot as plt

from geostack.raster import Raster
from geostack.vector import Vector
from geostack.solvers import Particle

N = 10000
steps = 1000
mu = 0.1
sigma = 0.01
t_start = 0
t_end = 10
angle = 45
s_x = 2.0
s_y = 6.0

# Create initial conditions
p = Vector()
for i in range (0, N):
    p.addPoint( [0, 0, 0, 0] )

# Create solver
dt = float(t_end-t_start)/steps
config = {
    "dt": dt,
    "updateScript" : [f'''
        mu.x = {mu};
        mu.y = {mu};
    ''', f'''
        sigma.x = {sigma};
        sigma.y = {sigma};
    ''', f'''
        REAL sqrt_dt = sqrt(dt);
        REAL theta = radians({angle});
        REALVEC2 B = (REALVEC2)(
            randomNormal(0.0, sqrt_dt), 
            randomNormal(0.0, sqrt_dt));
        dW.x = dot((REALVEC2)( {s_x}*cos(theta), {s_y}*sin(theta)), B);
        dW.y = dot((REALVEC2)(-{s_x}*sin(theta), {s_y}*cos(theta)), B);
    ''']
}
solver = Particle()
solver.init(json.dumps(config), p)

# Run solver  
for step in range(0, steps):

    # Take step
    solver.step()
    
# Refresh Vector RTree
p.buildTree()
    
# Built lists
x = []
y = []
for i in range(0, N):
    x.append(p.getCoordinate(i)[0])
    y.append(p.getCoordinate(i)[1])

# Create distance map
dist = p.mapDistance(0.01, '''
    REAL rb = exp(-1000.0*dot(d, d));
    output = isValid_REAL(output) ? output+rb : rb;
    ''')
distBounds = dist.getDimensions().to_dict()
extent = (distBounds['dim']['ox'], distBounds['ex'], 
    distBounds['dim']['oy'], distBounds['ey'])

# Plot
plt.xlabel("x")
plt.ylabel("y")
plt.plot(x, y, 'ko', markersize=0.5, alpha=0.25)
#plt.axis((0, 2, 0, 2))
plt.imshow(dist.data[::-1], cmap=plt.cm.viridis, extent=extent)
plt.show()

print("Done")
