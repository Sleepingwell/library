# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import sys

from geostack.raster import Raster
from geostack.runner import stipple, runScript
from geostack.io import vectorToGeoJson

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(nx = 10, ny = 10, hx = 10.0, hy = 10.0)

runScript("A = x*y;", [A])

# Write Raster
A.write('./_out_stipple.tiff')

# Run script
v = stipple("x += count*0.1; y += count*0.1; aaa = A; bbb = x; create = A < 25*25;", [A], ["aaa", "bbb"], 3)

# Write vector
with open('./_out_stipple.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(v, enforceProjection = False))
    
print("Done")
