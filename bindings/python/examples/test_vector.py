# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from geostack.vector import Vector

# Create vector layer
v = Vector()

# Create point
point_id = v.addPoint( [0.5, 1.5] )

# Get point coordinates
c = v.getPointCoordinate(point_id)
print(c)

print(c[0])
print(c[1])
print(c[2])
print(c[3])

print(c.p)
print(c.q)
print(c.r)
print(c.s)

# Create line string
line_id = v.addLineString( [ [0, 0], [1, 1], [2, 0], [3, 1] ] )

# Get line string coordinates
c = v.getLineStringCoordinates(line_id)
print(c)

# Create polygon
polygon_id = v.addPolygon( [
    [[0, 0], [1, 0], [1, 1], [0, 1]], 
    [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25]]
    ] )

# Get polygon coordinates
c = v.getPolygonCoordinates(polygon_id)
print(c)

# Get counts
print(v.getPointCount())
print(v.getLineStringCount())
print(v.getPolygonCount())

# Set properties
v.setProperty(point_id, "property 1", "this is a string")
v.setProperty(point_id, "property 2", 99)
v.setProperty(line_id, "property 3", [1.1, 2.2, 3.3, 4.5])

# Get properties
print(v.getProperty(point_id, "property 1"))
print(v.getProperty(point_id, "property 2"))
print(v.getProperty(line_id, "property 3"))

# Convert type
v.setProperty(point_id, "property 4", "77.7")
print(v.getProperty(point_id, "property 4", int))

# Attempt to convert type or get missing property -> throw exception
#print(v.getProperty(point_id, "property 1", int))
#print(v.getProperty(point_id, "no property"))

# Set multiple properties
for idx in v.getGeometryIndexes():
    v.setProperty(idx, "property 5", 6)
    
for idx in v.getLineStringIndexes():
    v.setProperty(idx, "property 6", 7)
    
# Get properties
print(v.getProperty(line_id, "property 5"))
print(v.getProperty(line_id, "property 6"))

