# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import random
from geostack.vector import Vector
from geostack.io import vectorToGeoJson

# Create random point set
v = Vector()
for i in range(0, 100):
    idx = v.addPoint( [ random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0) ] )
    v.setProperty(idx, "value", random.uniform(0.0, 1.0))

with open("_points.geojson", 'w') as fp:
    fp.write(vectorToGeoJson(v, enforceProjection = False))

# Standard distance
distance = v.mapDistance(0.01)
distance.write("_points_distance.tiff")

# Radial basis
distance = v.mapDistance(0.01, '''
    REAL r = exp(-100.0*dot(d, d));
    if (isValid_REAL(output)) {
        output += r;
    } else {
        output = r;
    }''')
distance.write("_points_distance_radial.tiff")

# Inverse distance weighting
distance = v.mapDistance(0.01, script='''
  REAL iDist = pow(length(d)+1.0E-6, -4.0);
  if (isValid_REAL(output)) {
    denom += iDist;
    output += value*iDist;
  } else {
    denom = iDist;
    output = value*iDist;
  }
''')
distance.write("_points_distance_idw.tiff")