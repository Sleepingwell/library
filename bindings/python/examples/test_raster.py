# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import sys
import numpy as np

from geostack.raster import Raster
from geostack.runner import runScript
from geostack.gs_enums import ReductionType

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(nx = 5, ny = 5, hx = 1.0, hy = 1.0)

# Set all cell values
A.setAllCellValues(2.0)

# Set single cell value
A.setCellValue(5.0, 2, 2)

# Output all data
print("All data in A:")
print(A.data)
print()

# Output data at single cell
print("Data in A at (2, 2):")
print(A.getCellValue(2, 2))
print()

# Increment every cell by one
print("Adding 1 to A: 'A = A + 1;'")
runScript("A = A + 1;", [A])

# Output all data
print("All data in A:")
print(A.data)
print()

# Create raster layer
B = Raster(name = "B")

# Initialize Raster
B.init(nx = 10, ny = 10, hx = 1.0, hy = 1.0)

# Populate raster
runScript("B = x;", [B])

# Output all data
print("All data in B:")
print(B.data)
print()

# Overwrite B with A
print("Overwrite B with A in valid cells: 'B = isValid_REAL(A) ? A : B;'")
runScript("B = isValid_REAL(A) ? A : B;", [B, A])

# Output all data
print("All data in B:")
print(B.data)
print()

# Add A and B
print("Add A and B: 'output = A + B;'")
C = runScript("output = A + B;", [A, B])
C.name = 'C'

# Output all data
print("All data in C:")
print(C.data)
print()

# Reduce
print("Reduce output and B: 'output = A + B;'")
B.setReductionType(ReductionType.Sum)
C = runScript("output = A + B;", [A, B], ReductionType.Sum)
C.name = 'C'
print(f"Sum of B: {B.reduceVal}")
print(f"Sum of C: {C.reduceVal}")
print()

print("Done")

