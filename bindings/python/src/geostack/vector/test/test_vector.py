# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import json
import numpy as np
from time import time
import pytest
from distutils import dir_util
sys.path.insert(0, os.path.realpath('../../../'))

from geostack.io import geoJsonToVector, vectorToGeoJson
from geostack.gs_enums import GeometryType
from geostack.vector import vector


@pytest.fixture
def vector_object():
    c = vector.Coordinate.from_list([144.9631, -37.8136])
    vec = vector.Vector()
    pointIdx = vec.addPoint(c)
    vec.setProperty(pointIdx, "newproperty", "newstr")
    return vec


def test_vector_initialization():
    c = vector.Coordinate.from_list([144.9631, -37.8136])
    vec = vector.Vector()
    pointIdx = vec.addPoint(c)
    vec.setProperty(pointIdx, "newproperty", "newstr")
    assert vec.getPointCoordinate(pointIdx) == c
    assert vec.getProperty(pointIdx, "newproperty", str) == "newstr"


def test_vector_copy(vector_object):
    v2 = vector.Vector(vector_object)
    assert v2.getGeometryIndexes().size == vector_object.getGeometryIndexes().size


def test_vector_assign(vector_object):
    v2 = vector.Vector.assign(vector_object)
    assert v2.getGeometryIndexes().size == vector_object.getGeometryIndexes().size
