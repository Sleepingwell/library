#include <functional>
#include <string>

#if defined(_WIN32)
#  define DLL00_EXPORT __declspec(dllexport)
#else
#  define DLL00_EXPORT
#endif

namespace Geostack
{
    using proj4getter = std::function<std::string(std::size_t)>;

    // function called from C++
    std::string Proj4FromEPSG(proj4getter, std::size_t);

    // function called from python
    DLL00_EXPORT void callback_Proj4FromEPSG(void*, std::function<std::string(void*, std::size_t)>,
                                std::size_t);
} // namespace geostack
