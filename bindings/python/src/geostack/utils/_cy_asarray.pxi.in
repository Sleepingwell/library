"""
Template for each `dtype` helper function

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# x_name, f_name, x_type

rtypes = [('dbl', 'double', "float64"),
          ('flt', 'float', "float32"),
          ('u32', 'uint32_t', "uint32"),
          ('u64', 'uint64_t', "uint64"),
          ('i32', 'int32_t', "int32"),
          ('i64', 'int64_t', "int64"),]

}}

{{for x_name, x_type, np_type in rtypes}}

cdef class {{x_name}}_vector_as_array:
    def __cinit__({{x_name}}_vector_as_array self):
        self.buf = NULL

    def __init__({{x_name}}_vector_as_array self):
        self.buf = new vector[{{x_type}}]()

    def __dealloc__({{x_name}}_vector_as_array self):
        if self.buf != NULL:
            del self.buf

    cdef void set_data({{x_name}}_vector_as_array self,
                       vector[{{x_type}}]& other) except+:
        if self.buf != NULL:
            del self.buf
        self.buf = new vector[{{x_type}}](other)

    def ndarray({{x_name}}_vector_as_array self):
        cdef np.intp_t n
        cdef {{x_type}}* _data
        cdef np.uintp_t uintptr

        n = <np.intp_t> self.buf.size()
        if NPY_LIKELY(n > 0):
            _data = self.buf.data()
            uintptr = <np.uintp_t> (<void*> _data)
            dtype = np.dtype(np.{{np_type}})
            self.__array_interface__ = dict(
                data = (uintptr, False),
                descr = dtype.descr,
                shape = (n,),
                strides = (dtype.itemsize,),
                typestr = dtype.str,
                version = 3,
            )
            return np.asarray(self)
        else:
            return np.empty(shape=(0,), dtype=np.{{np_type}})
{{endfor}}