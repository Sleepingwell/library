#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: cdivision=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libc.stdint cimport uint32_t, uint64_t, int32_t, int64_t
from libcpp.vector cimport vector
import numpy as np
cimport numpy as np
from ..core.cpp_tools cimport MatrixXd

np.import_array()

include "_cy_asarray.pxi"


cpdef test(int nx, int ny):
    cdef MatrixXd m = MatrixXd.Random(nx, ny)
    print(m(0, 0))

cdef void set_matrix_element(MatrixXd& m, int row, int col, double elm):
    cdef double* d = &(m(row,col))
    d[0] = elm