# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3

from cython.operator import dereference as deref
from libcpp.memory cimport shared_ptr, unique_ptr
from libc.stdint cimport uint8_t, uint32_t, uint64_t, int32_t, int64_t
from libcpp import nullptr_t, nullptr
from libcpp.string cimport string
from libcpp cimport bool

cdef extern from "utils.h":
    void cy_copy[T](T& a, T& b) nogil

cdef extern from "gs_solver.h" namespace "Geostack":
    cdef enum VerbosityType "Geostack::Verbosity::Type":
        NotSet = 0
        Debug = 10
        Info = 20
        Warning = 30
        Error = 40
        Critical = 50

cdef extern from "gs_solver.h" namespace "Geostack":
    bool isValid[T](T a) nogil except+
    bool isInvalid[T](T a) nogil except+

cdef extern from "gs_solver.h" namespace "Geostack":
    cdef cppclass Solver:
        Solver() nogil except +
        void setVerbose(bool verbose_) nogil except+
        void setVerboseLevel(uint8_t verbose_) nogil except+
        uint8_t getVerboseLevel() nogil except+

        @staticmethod
        Solver& getSolver() nogil except+
        string getError() nogil except+
        bool openCLInitialized() except+
        size_t buildProgram(string uid, string clProgram) except+
        int getMaxWorkGroupSize(string) nogil except+
        int getAlignMemorySize(string) nogil except+
        void resetTimers() except+

        void switchTimers(string, string) except+
        void incrementTimers(string, string, double) except+
        void displayTimers() except+
        string currentTimer(string) except+
        void setHostMemoryLimit(uint64_t hostMemoryLimit_) nogil except+
        uint64_t getHostMemoryLimit() nogil except+
        void setDeviceMemoryLimit(uint64_t deviceMemoryLimit_) nogil except+
        uint64_t getDeviceMemoryLimit() nogil except+

        @staticmethod
        string processScript(string script) nogil except+
        @staticmethod
        size_t getNullHash() nogil except+
        @staticmethod
        string getTypeString[R]() except+
        @staticmethod
        string getOpenCLTypeString[R]() except+

cdef class cySolver:
    cdef Solver* thisptr
    cpdef void setVerbose(self, bool verbose_) except+
    cpdef void setVerboseLevel(self, uint8_t verbose_) except+
    cpdef uint8_t getVerboseLevel(self) except+
    cpdef void setHostMemoryLimit(self, uint64_t hostMemoryLimit_) except+
    cpdef uint64_t getHostMemoryLimit(self) except+
    cpdef void setDeviceMemoryLimit(self, uint64_t deviceMemoryLimit_) except+
    cpdef uint64_t getDeviceMemoryLimit(self) except+
    cpdef string getError(self) except+
    cpdef string processScript(self, string script) except+
    cpdef size_t getNullHash(self) except+

cpdef bool isValid_flt(float a) except+
cpdef bool isValid_dbl(double a) except+
cpdef bool isValid_u32(uint32_t a) except+
cpdef bool isValid_u64(uint64_t a) except+
cpdef bool isValid_i32(int32_t a) except+
cpdef bool isValid_i64(int64_t a) except+
cpdef bool isValid_str(string a) except+

cpdef bool isInvalid_flt(float a) except+
cpdef bool isInvalid_dbl(double a) except+
cpdef bool isInvalid_u32(uint32_t a) except+
cpdef bool isInvalid_u64(uint64_t a) except+
cpdef bool isInvalid_i32(int32_t a) except+
cpdef bool isInvalid_i64(int64_t a) except+
cpdef bool isInvalid_str(string a) except+