# Cython wrapper for Geostack library

## Description

This repository contains the Cython wrapper for Geostack library. For now, only 
a small piece of code has been written to provide basic functionality of the 
raster and vector class to Python.

The wrapper code in written in [**_cy_vector.pyx**](./PyGeoStack/vector/_cy_vector.pyx) and [**_cy_raster.pyx**](./PyGeoStack/raster/_cy_raster.pyx) and the 
header declaration are provided in [**_cy_vector.pxd**](./PyGeoStack/vector/_cy_vector.pxd), [**_cy_raster.pxd**](./PyGeoStack/raster/_cy_raster.pyx). At the moment, 
the wrapper tries to get most of the functions in Cython, however, in time, 
it needs to be worked on, so that some of the functions which are not needed 
by Cython/Python are removed.

Additionally, there is a Python wrapper around the Cython which provides a more 
intuitive and object oriented interface for GeoStack.

## Requirements

1. Python 3.6 or greater (code works with Python 2.7 too)
2. Cython 0.27.1 (at least)
3. g++ or MSVC compiler
4. numpy

## Compile

In order to compile and test this code, you need to provide the correct paths 
in the [**setup.py**](./setup.py) file. I haven't yet worked on finding the paths variable 
within the [**setup.py**](./setup.py) file. I believe it can be done, however, 
as this is my first few commits so I don't want to go into so much detail.

At any rate, this project will move away from vanilla Cython build to using 
Cmake, thus making the process of finding paths much easier.

Before compiling, it is necessary to find out the paths of the following libraries:

1. libGeoStackShared

```bash
  python setup.py build_ext -if
```

If all goes well, you should have a series of shared library built in your 
current directory.

## Example

As the library built by this project is a shared library which dynamically links 
to the geostack shared library, therefore, it is important 
that python should be able to locate those libraries during runtime. Therefore, it 
is important that path of all the dependency are added to **LD_LIBRARY_PATH** 
variable (on posix based systems). If you have all of those paths added, 
there should be no problem using this piece of code.

```python
from raster import Raster, runScript
import numpy as np


```

In the above example, I just import raster class, which will import the 
necessary Cython implementation of Raster, which therein imports the GeoStack
library. 

As it can be noticed that this code doesn't do anything useful right now, it 
just takes imports libraries. A collection of better examples are shown in the [**test**](./tests) 
directory, which shows how to use the `Raster` class

This code base is under active development, where the code will be restructured
and modified. 


**author**: Nikhil Garg
**email**: nikhil.garg@data61.csiro.au
