geostack.series package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 2


Submodules
----------

geostack.series.series module
-----------------------------

.. automodule:: geostack.series.series
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:

Module contents
---------------

.. automodule:: geostack.series
   :members:
   :undoc-members:
   :show-inheritance:
