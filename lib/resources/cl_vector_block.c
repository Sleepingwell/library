/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

/**
* General vector operator
*/
__kernel void vector(
    const uint _gn,
    __global uint *_gid/*__ARGS__*/
) {

    // Get geometry index
    const uint _id = get_global_id(0);

    // Check limits
    if (_id < _gn) {

        // Get property index
        const size_t _index = _gid[_id];
        
/*__BOUNDS__
        // Get geometry bounding box
        const Coordinate boundsMin = _gb[_index*2];
        const Coordinate boundsMax = _gb[_index*2+1];
__BOUNDS__*/

        // Keep flag
        bool keep = true;
        
/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------
        
        // Apply filter
        if (keep) {
/*__POST__*/
        } else {
            _gid[_id] = noData_UINT;
        }
    }
}

