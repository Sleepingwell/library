/**
* Shallow water initialisation
*/
__kernel void init(
REAL ic2,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // Set height nodata values to zero
        if (isInvalid_REAL(h)) h = 0.0;

        // Set unit discharge to zero
        uh = 0.0;
        vh = 0.0;

        // Calculate equilibrium values - velocity is zero
        const REAL g = 9.81;
        REAL gh2 = g*h*h;
        _val3D_a(__fi, _i, _j, 0) = h-(5.0/6.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 1) = (1.0/6.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 2) = (1.0/6.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 3) = (1.0/6.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 4) = (1.0/6.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 5) = (1.0/24.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 6) = (1.0/24.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 7) = (1.0/24.0)*gh2*ic2;
        _val3D_a(__fi, _i, _j, 8) = (1.0/24.0)*gh2*ic2;

/*__POST__*/

    }

}

