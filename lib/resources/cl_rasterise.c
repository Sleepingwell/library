/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Aligned access
#define _pval2D_a(R, _i, _j) (((R)+(_i)+(_j)*_dim.mx))
#define _val3D_a(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim.my)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

// Orientation test for three points
REAL orient2D(
    REAL x0, REAL y0, 
    REAL x1, REAL y1, 
    REAL x2, REAL y2) {
    return (x0-x2)*(y1-y2)-(y0-y2)*(x1-x2);
}

// Function to test if bounding box b intersects bounding box t
bool intersectsBoundingBox2D(
    REAL bx_min, REAL by_min,
    REAL bx_max, REAL by_max,
    REAL tx_min, REAL ty_min,
    REAL tx_max, REAL ty_max) {
    return (bx_min <= tx_max && bx_max >= tx_min && by_max >= ty_min && by_min <= ty_max);
}

// Function to test if two line segments (x0, y0), (x1, y1) and (x2, y2), (x3, y3) intercept
bool intercepts2D( 
    REAL x0, REAL y0, 
    REAL x1, REAL y1, 
    REAL x2, REAL y2, 
    REAL x3, REAL y3) {        
    
    // Degenerate cases
    REAL dx01 = x0-x1;
    REAL dx02 = x0-x2;
    REAL dx23 = x2-x3;
    if (dx01 == 0.0 && dx23 == 0.0) {
        if (dx02 != 0.0) return false;
        return max(y0, y1) >= min(y2, y3) && max(y2, y3) >= min(y0, y1);
    }
    REAL dy01 = y0-y1;
    REAL dy02 = y0-y2;
    REAL dy23 = y2-y3;
    if (dy01 == 0.0 && dy23 == 0.0) {
        if (dy02 != 0.0) return false;
        return max(x0, x1) >= min(x2, x3) && max(x2, x3) >= min(x0, x1);
    }
    
    // Check for intercept
    REAL d = dx01*dy23-dy01*dx23;
    REAL tn = dx02*dy23-dy02*dx23;
    if (tn*d >= 0.0 && fabs(tn) <= fabs(d)) {
        REAL un = dx02*dy01-dy02*dx01;
        if (un*d >= 0.0 && fabs(un) <= fabs(d)) {
            return true;
        }
    }
    return false;
}

/**
* Rasterise points.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _p Array of offsets for properties.
* @param _n Number of coordinates.
*/
__kernel void rasterise2DPoint(
    __global TYPE *_u,
    const Dimensions _dim,
    __global Coordinate *_c, 
    __global uint *_p, 
    const uint _n/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);
    uint count = 0;

    // Check limits
    if (index < _n) {
    
/*__VARS__*/
    
/*__WRITE__
        // Set default output
        TYPE output = noData_TYPE;
__WRITE__*/
    
        // Get coordinate
        Coordinate _pc = *(_c+index);
        
/*__PROJECT__
        // Convert to raster projection
        convert(&_pc, &_rproj, &_vproj);
__PROJECT__*/

        // Convert to raster coordinates
        _pc.p = (_pc.p-_dim.ox)/_dim.hx;
        _pc.q = (_pc.q-_dim.oy)/_dim.hy;

        // Check position
        if (_pc.p < 0.0 || _pc.q < 0.0 || _pc.p >= (REAL)_dim.nx || _pc.q >= (REAL)_dim.ny) {
            return;
        }
        
        // Get coordinates
        size_t _i = (size_t)_pc.p;
        size_t _j = (size_t)_pc.q;
        
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox; 
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        
        const Coordinate cA = _pc;
        const Coordinate cB = cA; // Dummy second coordinate for points

        for (size_t _k = 0; _k < _dim.nz; _k++) {        
/*__LEVEL__*/
            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

/*__WRITE__
            // Write to raster
            _val3D_a(_u, _i, _j, _k) = output;
__WRITE__*/
        }
    }
}

/**
* Rasterise line strings.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _p Array of offsets for properties.
* @param _o Array of offsets denoting the start of each line string.
* @param _n Number of line strings.
*/
__kernel void rasterise2DLineString(
    __global TYPE *_u,
    const Dimensions _dim,
    __global Coordinate *_c,  
    __global uint *_p, 
    __global uint *_o,
    const uint _ln/*__ARGS__*/
) {

    // Get line index
    const size_t index = get_global_id(0);
    uint count = 0;

    // Check limits
    if (index < _ln) {
    
/*__VARS__*/
    
/*__WRITE__
        // Set default output
        TYPE output = noData_TYPE;
__WRITE__*/

        // Get offsets
        size_t _soff = index > 0 ? _o[index-1]+1 : 0;
        size_t _eoff = _o[index]+1; 

        // Get coordinate
        Coordinate _lc = *(_c+_soff);

/*__PROJECT__
        // Convert to raster projection
        convert(&_lc, &_rproj, &_vproj);
__PROJECT__*/

        // Convert to raster coordinates
        int _sx, _ex = (int)((_lc.p-_dim.ox)/_dim.hx);
        int _sy, _ey = (int)((_lc.q-_dim.oy)/_dim.hy);
    
        // Loop over lines
        for (size_t _l = _soff+1; _l < _eoff; _l++) {
        
            // Get coordinates
            _sx = _ex;
            _sy = _ey;
            const Coordinate cA = _lc;
            _lc = *(_c+_l);      
            const Coordinate cB = _lc;      

/*__PROJECT__
            // Convert to raster projection
            convert(&_lc, &_rproj, &_vproj);
__PROJECT__*/

            // Convert to indexes
            _ex = (int)((_lc.p-_dim.ox)/_dim.hx);
            _ey = (int)((_lc.q-_dim.oy)/_dim.hy);

            // Calculate line points
            int _dx = _ex-_sx;
            int _dy = _ey-_sy;
            if (_dx == 0 && _dy == 0) {

                // Special case for single point
                if (_sx >= 0 && _sx < _dim.nx && _sy >= 0 && _sy < _dim.ny) {
                
                    // Get coordinates
                    int _i = _sx;
                    int _j = _sy;

                    const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox; 
                    const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
                    
                    for (size_t _k = 0; _k < _dim.nz; _k++) {
/*__LEVEL__*/
                        // ---------------------------------
                        // User defined code
/*__CODE__*/
                        // ---------------------------------
                     
/*__WRITE__
                        // Write to raster
                        _val3D_a(_u, _i, _j, _k) = output;
__WRITE__*/
                    }
                }

            } else {

                if (abs(_dy) <= abs(_dx)) {

                    // Step in x
                    REAL _m = (REAL)_dy/(REAL)_dx;
                    int _inc = (_dx > 0) ? 1 : -1;
                    for (int _i = _sx ; _i != _ex ; _i += _inc) {
                        REAL _j = (REAL)_sy+_m*(_i-_sx);
                        if (_i >= 0 && _i < _dim.nx && _j >= 0.0 && _j < (REAL)_dim.ny) {

                            // Get coordinates
                            const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
                            const REAL y = (_j+0.5)*_dim.hy+_dim.oy;
                            
                            for (size_t _k = 0; _k < _dim.nz; _k++) {
/*__LEVEL__*/
                                // ---------------------------------
                                // User defined code
/*__CODE__*/
                                // ---------------------------------
                            
/*__WRITE__
                                // Write to raster
                                _val3D_a(_u, _i, (int)_j, _k) = output;
__WRITE__*/
                            }
                        }
                    }
                } else if (abs(_dy) > abs(_dx)) {

                    // Step in y
                    REAL _m = (REAL)_dx/(REAL)_dy;
                    int _inc = (_dy > 0) ? 1 : -1;
                    for (int _j = _sy ; _j != _ey ; _j += _inc) {
                        REAL _i = (REAL)_sx+_m*(_j-_sy);
                        if (_i >= 0.0 && _i < (REAL)_dim.nx && _j >=0 && _j < _dim.ny) {

                            // Get coordinates
                            const REAL x = (_i+0.5)*_dim.hx+_dim.ox;
                            const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
                            
                            for (size_t _k = 0; _k < _dim.nz; _k++) {
/*__LEVEL__*/
                                // ---------------------------------
                                // User defined code
/*__CODE__*/
                                // ---------------------------------

/*__WRITE__
                                // Write to raster
                                _val3D_a(_u, (int)_i, _j, _k) = output;
__WRITE__*/
                            }
                        }
                    }
                }
            }
        }
    }
}

/**
* Rasterise polygons.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _p Array of offsets for properties.
* @param _o Array of offsets denoting the start of each polygon, must include terminator of 0.
* @param _b Array of polygon bounding boxes as coordinates.
* @param _v Local array of output values at each level.
* @param _n Total number of polygons
*/
__kernel void rasterise2DPolygon(
    __global TYPE *_u,
    const Dimensions _dim,
    __global REALVEC4 *_c, 
    __global uint *_p, 
    __global int *_o,
    __global REALVEC4 *_b,
    const uchar _params,
    __local REAL *_v,
    const uint _n/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);

    // Intersection checks
    const uchar _interior = _params&0x01;
    const uchar _intersect = _params&0x02;

    const Coordinate cA; // Dummy for script compilation
    const Coordinate cB; // Dummy for script compilation

/*__WRITE__
    // Set default output
    for (size_t _k = 0; _k < _dim.nz; _k++) {
        *(_v+get_local_id(0)+_k*get_local_size(0)) = noData_TYPE;
    }
__WRITE__*/

    // Check limits
    bool isValid = _n != 0 && _i < _dim.nx && _j < _dim.ny;
    if (isValid) {
        
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

        // Calculate cell bounding box
        REAL _bx_min = (REAL)_i*_dim.hx+_dim.ox;
        REAL _by_min = (REAL)_j*_dim.hy+_dim.oy;
        REAL _bx_max = _bx_min+_dim.hx;
        REAL _by_max = _by_min+_dim.hy;

/*__PROJECT__
* 
        // Convert to vector projection
        Coordinate _b_min;
        _b_min.p = _bx_min;
        _b_min.q = _by_min;
        convert(&_b_min, &_vproj, &_rproj);
        
        Coordinate _b_max;
        _b_max.p = _bx_max;
        _b_max.q = _by_max;
        convert(&_b_max, &_vproj, &_rproj);

        _bx_min = min(_b_min.p, _b_max.p);
        _by_min = min(_b_min.q, _b_max.q);
        _bx_max = max(_b_min.p, _b_max.p);
        _by_max = max(_b_min.q, _b_max.q);

__PROJECT__*/

        // Sample point
        const REALVEC2 _pos = (REALVEC2)(_bx_min, _by_min);

        // Internal variables
        bool _found = false; // Polygon interception found
        uint count = 0;      // Count of polygons in cell
        int _sign = 1;       // Sign of winding, 0 if outside polygon
        uint _off = 0;       // Coordinate offset
        uint _poly = 0;      // Total polygon count
        uint index = 0;      // Top-level polygon index

        // Vector coordinates
        REALVEC2 c0, c1;

        // Loop over polygons
        do {

            // Loop over sub-polygons
            do {

                // Process if cell is within polygon bounding box
                if (!_found && intersectsBoundingBox2D(_bx_min, _by_min, _bx_max, _by_max, 
                    _b[_poly*2].x, _b[_poly*2].y, _b[_poly*2+1].x, _b[_poly*2+1].y)) {

                    // Check if point is within polygon
                    int _winding = 0;
                    uint _off_next = _off+abs(_o[_poly])-1;

                    while (_off < _off_next) {
            
                        // Get coordinates
                        c0 = _c[_off].xy;
                        c1 = _c[_off+1].xy;

                        if (orient2D(_bx_min, _by_min, _bx_min, _by_max, c0.x, c0.y) <= 0.0 &&
                            orient2D(_bx_min, _by_max, _bx_max, _by_max, c0.x, c0.y) <= 0.0 &&
                            orient2D(_bx_max, _by_max, _bx_max, _by_min, c0.x, c0.y) <= 0.0 &&
                            orient2D(_bx_max, _by_min, _bx_min, _by_min, c0.x, c0.y) <= 0.0) {

                            // Mark as found if any coordinate is within bounding box and intersection check is set
                            if (_intersect) _found = true;
                            _off = _off_next;
                            _winding = 0;
                            break;
                        
                        } else if (
                            intercepts2D(_bx_min, _by_min, _bx_min, _by_max, c0.x, c0.y, c1.x, c1.y) || 
                            intercepts2D(_bx_min, _by_max, _bx_max, _by_max, c0.x, c0.y, c1.x, c1.y) || 
                            intercepts2D(_bx_max, _by_max, _bx_max, _by_min, c0.x, c0.y, c1.x, c1.y) ||
                            intercepts2D(_bx_max, _by_min, _bx_min, _by_min, c0.x, c0.y, c1.x, c1.y)) {

                            // Mark as found if any edge intercepts cell bounding box and intersection check is set
                            if (_intersect) _found = true;
                            _off = _off_next;
                            _winding = 0;
                            break;

                        } else if (_interior) {
                        
                            // Check if bounding box point lies inside polygon
                            REALVEC2 l = c1-c0;
                            REALVEC2 v = _pos-c0;

                            // Check _winding (http://geomalgorithms.com/a03-_inclusion.html)
                            if (c0.y <= _pos.y) {
                                if (c1.y > _pos.y && l.x*v.y > v.x*l.y)
                                    _winding++;
                            } else {
                                if (c1.y <= _pos.y && l.x*v.y < v.x*l.y)
                                    _winding--;
                            }
                        }

                        // Increment coordinate
                        _off++;
                    }

                    // Increment coordinate
                    _off++;
            
                    // If the point is inside any polygon, flip the sign
                    if (_winding != 0) {
                        _sign = -_sign;
                    }

                } else {

                    // Skip polygon
                    _off+=abs(_o[_poly]);
                }

                // Increment polygon counter
                _poly++;

            } while(_o[_poly] < 0);

            // Set index if any polygons are found
            if (_found || _sign < 0) {
/*__VARS__*/
                count++;
                for (size_t _k = 0; _k < _dim.nz; _k++) {
/*__LEVEL__*/
                    REAL output = *(_v+get_local_id(0)+_k*get_local_size(0));
                    // ---------------------------------
                    // User defined code
/*__CODE__*/
                    // ---------------------------------
                    *(_v+get_local_id(0)+_k*get_local_size(0)) = output;
                }
            }

            // Increment top-level polygon counter
            index++;
            
            // Reset flags
            _found = false;
            _sign = 1;

        } while( _poly < _n);
    }

    // Barrier before write
    barrier(CLK_GLOBAL_MEM_FENCE);
    
/*__WRITE__
    // Write if output is non-nodata value
    if (isValid) {
        
        for (size_t _k = 0; _k < _dim.nz; _k++) {

            // Write to raster
            REAL output = *(_v+get_local_id(0)+_k*get_local_size(0));
            if (isValid_TYPE(output)) {
                _val3D_a(_u, _i, _j, _k) = output;
            }
        }
    }
__WRITE__*/
}

