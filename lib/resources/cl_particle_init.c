/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

/**
* Particle initialisation operation
*/
__kernel void init(
    __global Coordinate *_cp,
    __global Coordinate *_vp,
    const uint _start,
    const uint _end,
    const uint seed,
    __global RandomState *_rs/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = _start+get_global_id(0);

    // Check limits
    if (index < _end) {

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;      // Random state pointer
    
        // Get variables
        Coordinate _c = *(_cp+index);
        Coordinate _v = *(_vp+index);

        // Seed random variable
        random_init(_prsl, seed, index);
        
        REAL time = _c.s;
        REAL radius = 0.0;  
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);
        REALVEC3 velocity = (REALVEC3)(0.0, 0.0, 0.0);
        
/*__VARS__*/

    // ---------------------------------
    // User defined code
/*__CODE__*/
    // ---------------------------------

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;
        _v.p = velocity.x;
        _v.q = velocity.y;
        _v.r = velocity.z;
        _v.s = radius;

        *(_cp+index) = _c;
        *(_vp+index) = _v;

        // Store state
        *(_rs+index) = _rsl;

/*__POST__*/
    }
}

