/**
* Level set step advect
*/

__kernel void advect(
uint _c,
__global uint *_ric,
__global RasterIndex *_ri,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24;

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // --------------------------------- 

        // Set default rate to zero
        _rate = 0.0;
        
        // Time integrate with Euler step
        _distance_update = _distance;

        // Store speed
        _speed = 0.0;  
        
        // Store narrow band indexes
        if (fabs(_distance) < band_width) {        

            // Create narrow band index
            RasterIndex ri;
            ri.i = (ushort)_i;
            ri.j = (ushort)_j;
            ri.k = (ushort)_k;
            ri.t = start_time; // Needed to capture external updates

            // Update start time     
            if (isInvalid_REAL(start_time)) {
                REAL _nav = 0.0;
                ri.t = 0.0;
                if (isValid_REAL(start_time_N)) {
                    ri.t += start_time_N;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_NE)) {
                    ri.t += start_time_NE;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_E)) {
                    ri.t += start_time_E;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_SE)) {
                    ri.t += start_time_SE;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_S)) {
                    ri.t += start_time_S;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_SW)) {
                    ri.t += start_time_SW;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_W)) {
                    ri.t += start_time_W;
                    _nav += 1.0;
                }
                if (isValid_REAL(start_time_NW)) {
                    ri.t += start_time_NW;
                    _nav += 1.0;
                }
                if (_nav > 0.0) {
                    ri.t /= _nav;
                } else {                
                    ri.t = time;
                }
            }

            // Add to narrow band index
            atomic_inc(_ric+_c+1);
            *(_ri+atomic_inc(_ric)) = ri;
        }   

/*__POST__*/

    }
}

__kernel void advectInactive(
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------  

/*__POST__*/

    }
}

