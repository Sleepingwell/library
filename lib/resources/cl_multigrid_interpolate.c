/**
* Interpolate residual
*/
__kernel void interpolate(
__global REAL *_l0c,
__global REAL *_l0c_N,
__global REAL *_l0c_NE,
__global REAL *_l0c_E,
__global REAL *_l0f,
__global REAL *_l0f_N,
__global REAL *_l0f_NE,
__global REAL *_l0f_E
) {

    // Raster indexes
    const size_t _ic = get_global_id(0);
    const size_t _jc = get_global_id(1);

    // Read coarse layer
    REAL l0c = _val2D_a(_l0c, _ic, _jc);
    REAL l0c_N = (_jc == /*__MSIZE__*/-1) ? _val2D_a(_l0c_N, _ic, 0) : _val2D_a(_l0c, _ic, _jc+1);
    REAL l0c_E = (_ic == /*__MSIZE__*/-1) ? _val2D_a(_l0c_E, 0, _jc) : _val2D_a(_l0c, _ic+1, _jc);
    REAL l0c_NE = ((_jc == /*__MSIZE__*/-1) && (_ic == /*__MSIZE__*/-1)) ? _val2D_a(_l0c_NE, 0, 0) : \
        ((_ic == /*__MSIZE__*/-1) ? _val2D_a(_l0c_E,  0, _jc+1) : ((_jc == /*__MSIZE__*/-1) ? _val2D_a(_l0c_N, _ic+1, 0) : _val2D_a(_l0c, _ic+1, _jc+1)));
    
    // Get fine layer indexes
    size_t _if = _ic<<1;
    size_t _jf = _jc<<1;
    
    // Check quadrant and shift
    if (_if > /*__MSIZE__*/-1 && _jf > /*__MSIZE__*/-1) {

        // Shift north-east
        _if -= /*__MSIZE__*/;
        _jf -= /*__MSIZE__*/;
        _l0f = _l0f_NE;
    
    } else if (_if > /*__MSIZE__*/-1) {

        // Shift east
        _if -= /*__MSIZE__*/;
        _l0f = _l0f_E;

    } else if (_jf > /*__MSIZE__*/-1) {

        // Shift north
        _jf -= /*__MSIZE__*/;
        _l0f = _l0f_N;
    }

    // Write to fine layer
    *(_l0f+_if+(_jf<</*__ROW_BSHIFT__*/)) += l0c;
    *(_l0f+_if+1+(_jf<</*__ROW_BSHIFT__*/)) += 0.5*(l0c+l0c_E);
    *(_l0f+_if+((_jf+1)<</*__ROW_BSHIFT__*/)) += 0.5*(l0c+l0c_N);
    *(_l0f+_if+1+((_jf+1)<</*__ROW_BSHIFT__*/)) += 0.25*(l0c+l0c_N+l0c_E+l0c_NE);
}

