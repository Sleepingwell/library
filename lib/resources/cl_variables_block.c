/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
* General variables operator
*/
__kernel void variables(
    const uint _vn,
    __global uint *_vo/*__ARGS__*/
) {

    // Get geometry index
    const uint _id = get_global_id(0);

    // Check limits
    if (_id < _vn) {
        
/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------
        
/*__POST__*/

    }
}

