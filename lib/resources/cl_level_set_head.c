typedef struct RasterIndexStruct {

    // Coordinates
    ushort i;
    ushort j;
    ushort k;
    REAL t;
    
} __attribute__ ((aligned (8))) RasterIndex;

