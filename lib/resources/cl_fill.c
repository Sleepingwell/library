/**
* Fill buffer with REAL value
*/
__kernel void fill_REAL(
    __global REAL *_b,
    REAL _v,
    uint _N
) {
    const size_t _i = get_global_id(0);

    // Check limits
    if (_i < _N) {

        // Write value
        *(_b+_i) = _v;
    }
}

/**
* Fill buffer with CHAR value
*/
__kernel void fill_CHAR(
    __global char *_b,
    char _v,
    uint _N
) {
    const size_t _i = get_global_id(0);

    // Check limits
    if (_i < _N) {

        // Write value
        *(_b+_i) = _v;
    }
}

/**
* Fill buffer with UCHAR value
*/
__kernel void fill_UCHAR(
    __global uchar *_b,
    uchar _v,
    uint _N
) {
    const size_t _i = get_global_id(0);

    // Check limits
    if (_i < _N) {

        // Write value
        *(_b+_i) = _v;
    }
}

/**
* Fill buffer with INT value
*/
__kernel void fill_INT(
    __global int *_b,
    int _v,
    uint _N
) {
    const size_t _i = get_global_id(0);

    // Check limits
    if (_i < _N) {

        // Write value
        *(_b+_i) = _v;
    }
}

/**
* Fill buffer with UINT value
*/
__kernel void fill_UINT(
    __global uint *_b,
    uint _v,
    uint _N
) {
    const size_t _i = get_global_id(0);

    // Check limits
    if (_i < _N) {

        // Write value
        *(_b+_i) = _v;
    }
}