/**
* Reduce indexed components in Raster
*/
__kernel void reduce(
__global REALVEC4 *_reduce_vec,
__global UINT *_idx,
/*__ARGS__*/
) {    
    const size_t _j = get_global_id(0);

    // Check limits
    if (_j < _dim.ny) {

        // Local y-coordinate
        const REAL cy = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

        // Loop over row
        for (uint _i = 0; _i < _dim.nx; _i++) {
            uint idx = _val2D_a(_idx, _i, _j);
            if (isValid_UINT(idx)) {

/*__VARS__*/

                // Local x-coordinate
                REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
                REAL y = cy;
                REAL weight = 1.0;

/*__CODE__*/

                // Find average midpoint
                *(_reduce_vec+idx*get_global_size(0)+_j) += 
                    (REALVEC4)(weight*x, weight*y, weight, /*__VAR0__*/);
            }
        }
    }
}
