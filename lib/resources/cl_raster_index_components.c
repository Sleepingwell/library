/**
* Index components in Raster
* Calculations are run over sub-tiles in local workgroups
*/

#define val2D_c(R, i, j) (*((R)+(i)+(j)*get_local_size(0)))

// Binary search
UINT search(const UINT v, __global UINT *_idx_array, const UINT N) {

    UINT iL = 0;
    UINT iR = N-1;
    while (iL <= iR) {
        UINT i = iL+((iR-iL)>>1);
        if (_idx_array[i] == v) {
            return i;
        }
        if (_idx_array[i] < v) {
            iL = i+1;
        } else {
            iR = i-1;
        }
    }
    return noData_UINT;
}

// Union-find
UINT find_local(volatile UINT idx, __local UINT *_idxl) {

    // Find with path compression
    while (idx != *(_idxl+idx)) {
        *(_idxl+idx) = *(_idxl+*(_idxl+idx));
        idx = *(_idxl+idx);
    }
    return idx;
}

// Union merge
// From: A Study on Connected Components Labeling algorithms using GPUs, Oliveira and Lotufo (2010)
void merge_local(volatile UINT idx, volatile UINT idx_merge, __local UINT *_idxl) {

    while (true) {
        idx = find_local(idx, _idxl);
        idx_merge = find_local(idx_merge, _idxl);
        if (idx < idx_merge) {
            atomic_min(_idxl+idx_merge, idx);
        } else if (idx > idx_merge) {
            atomic_min(_idxl+idx, idx_merge);
        } else {
            break;
        }
    }
}

/**
* Group connected components over work-group
*/
__kernel void indexComponents(
__local UINT *_idxl,
__global UINT *_idx,
/*__ARGS__*/
) {
    
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {

        const size_t _il = get_local_id(0);
        const size_t _jl = get_local_id(1);
    
/*__VARS__*/

        // Index tiles    
        uint lidx = _il+_jl*get_local_size(0);
        if (/*__CODE__*/) {
            val2D_c(_idxl, _il, _jl) = lidx;
        } else {
            val2D_c(_idxl, _il, _jl) = noData_UINT;
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        uint idx = val2D_c(_idxl, _il, _jl);
        if (isValid_UINT(idx)) {
            UINT idx_candidate;
            if (_il > 0) {
                idx_candidate = val2D_c(_idxl, _il-1, _jl);
                if (isValid_UINT(idx_candidate) && idx != idx_candidate) {
                    merge_local(idx, idx_candidate, _idxl);
                }
            }
            idx = val2D_c(_idxl, _il, _jl);

            if (_il < get_local_size(0)-1) {
                idx_candidate = val2D_c(_idxl, _il+1, _jl);
                if (isValid_UINT(idx_candidate) && idx != idx_candidate) {
                    merge_local(idx, idx_candidate, _idxl);
                }
            }
            idx = val2D_c(_idxl, _il, _jl);

            if (_jl > 0) {
                idx_candidate = val2D_c(_idxl, _il, _jl-1);
                if (isValid_UINT(idx_candidate) && idx != idx_candidate) {
                    merge_local(idx, idx_candidate, _idxl);
                }
            }
            idx = val2D_c(_idxl, _il, _jl);

            if (_jl < get_local_size(1)-1) {
                idx_candidate = val2D_c(_idxl, _il, _jl+1);
                if (isValid_UINT(idx_candidate) && idx != idx_candidate) {
                    merge_local(idx, idx_candidate, _idxl);
                }
            }
            idx = val2D_c(_idxl, _il, _jl);
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        // Write
        if (isValid_UINT(idx)) {
            idx = find_local(idx, _idxl);
            UINT ix = idx%get_local_size(0);
            UINT iy = idx/get_local_size(0);
            UINT gidx = (get_group_id(1)*get_local_size(1)+iy)*_dim.mx+
                get_group_id(0)*get_local_size(0)+ix;
            _val3D_a(_idx, _i, _j, _k) = gidx;
        } else {
            _val3D_a(_idx, _i, _j, _k) = noData_UINT;
        }
    }
}

/**
* Find connected components between neighbouring workgroups and tiles
*/
__kernel void findComponents(
__global UINT *_idx,
__global UINT *_idx_W,
__global UINT *_idx_S,
__global UINT *_arr_count,
__global UINT *_arr_A,
__global UINT *_arr_B,
const Dimensions _dim
) {
    
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {

        const size_t _il = get_local_id(0);
        const size_t _jl = get_local_id(1);

        uint idx = _val3D_a(_idx, _i, _j, _k);
        if (isValid_UINT(idx)) {
    
            // West boundary processing
            if (_il == 0) {
                if (_i == 0) {
                    UINT idx_W = _val3D_a(_idx_W, _dim.mx-1, _j, _k);
                    if (isValid_UINT(idx_W)) {

                        // Set bit 32 to represent west tile
                        UINT aidx = atomic_inc(_arr_count);
                        if (idx < idx_W) {
                            *(_arr_A+aidx) = idx;
                            *(_arr_B+aidx) = (UINT)0x80000000 | idx_W;
                        } else {  
                            *(_arr_A+aidx) = (UINT)0x80000000 | idx_W;
                            *(_arr_B+aidx) = idx;
                        }
                    }
                } else {
                    UINT idx_W = _val3D_a(_idx, _i-1, _j, _k);
                    if (isValid_UINT(idx_W)) {
                        UINT aidx = atomic_inc(_arr_count);
                        if (idx < idx_W) {
                            *(_arr_A+aidx) = idx;
                            *(_arr_B+aidx) = idx_W;
                        } else {  
                            *(_arr_A+aidx) = idx_W;
                            *(_arr_B+aidx) = idx;
                        }
                    }
                }
            }
        
            // South boundary processing
            if (_jl == 0) {

                if (_j == 0) {
                    UINT idx_S = _val3D_a(_idx_S, _i, _dim.my-1, _k);
                    if (isValid_UINT(idx_S)) {

                        // Set bit 31 to represent south tile
                        UINT aidx = atomic_inc(_arr_count);
                        if (idx < idx_S) {
                            *(_arr_A+aidx) = idx;
                            *(_arr_B+aidx) = (UINT)0x40000000 | idx_S;
                        } else {  
                            *(_arr_A+aidx) = (UINT)0x40000000 | idx_S;
                            *(_arr_B+aidx) = idx;
                        }
                    }
                } else {
                    UINT idx_S = _val3D_a(_idx, _i, _j-1, _k);
                    if (isValid_UINT(idx_S)) {
                        UINT aidx = atomic_inc(_arr_count);
                        if (idx < idx_S) {
                            *(_arr_A+aidx) = idx;
                            *(_arr_B+aidx) = idx_S;
                        } else {  
                            *(_arr_A+aidx) = idx_S;
                            *(_arr_B+aidx) = idx;
                        }
                    }
                }
            }
        
            // Add root elements
            if (idx == _i+_j*_dim.mx) {
                UINT aidx = atomic_inc(_arr_count);
                *(_arr_A+aidx) = idx;
                *(_arr_B+aidx) = idx;
            }
        }
    }
}

/**
* Reindex components based on global map
*/
__kernel void reindex(
__global UINT *_idx,
__global UINT *_arr_A,
__global UINT *_arr_B,
UINT _arr_N,
const Dimensions _dim
) {
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {

        // Write index
        uint idx = _val3D_a(_idx, _i, _j, _k);
        if (isValid_UINT(idx)) {
            UINT i = search(idx, _arr_A, _arr_N);
            if (isValid_UINT(i)) {

                // Re-index
                _val3D_a(_idx, _i, _j, _k) = *(_arr_B+i);

            } else {

                // Search should always succeed
                _val3D_a(_idx, _i, _j, _k) = noData_UINT;
            }
        }
    }
}

/**
* Overwrite raster with index values
*/
__kernel void overwrite(
__global UINT *_idx,
/*__ARGS__*/
) {
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {
    
/*__VARS__*/

        // Write index
        uint idx = _val3D_a(_idx, _i, _j, _k);
        if (isValid_UINT(idx)) {
            /*__VAR0__*/ = idx;
        }

/*__POST__*/

    }
}
