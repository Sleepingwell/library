/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * This is adapted from code written by Jonathan Richard Shewchuk, 
 * released into public domain.
 */

#include <limits>

#include "gs_robust.h"

namespace Geostack {

    template<typename T>
    struct Constants {
        static constexpr T eps = std::numeric_limits<T>::epsilon()/2;

        static constexpr T splitter = (T)(1 << ((std::numeric_limits<T>::digits + 1) >> 1)) + 1.0;
        static constexpr T resulterrbound = (3.0 + 8.0 * Constants<T>::eps) * Constants<T>::eps;

        static constexpr T ccwerrboundA = (3.0 + 16.0 * Constants<T>::eps) * Constants<T>::eps;
        static constexpr T ccwerrboundB = (2.0 + 12.0 * Constants<T>::eps) * Constants<T>::eps;
        static constexpr T ccwerrboundC = (9.0 + 64.0 * Constants<T>::eps) * Constants<T>::eps * Constants<T>::eps;

        static constexpr T iccerrboundA = (10.0 + 96.0 * Constants<T>::eps) * Constants<T>::eps;
        static constexpr T iccerrboundB = (4.0 + 48.0 * Constants<T>::eps) * Constants<T>::eps;
        static constexpr T iccerrboundC = (44.0 + 576.0 * Constants<T>::eps) * Constants<T>::eps * Constants<T>::eps;
    };


    template<typename T>
    inline static T absolute(T a) {
        return (a) >= 0.0 ? (a) : -(a);
    }

    template<typename T>
    inline static void split(T a, T &ahi, T &alo) {
        T c = (T) (Constants<T>::splitter * a);
        T abig = (T) (c - a);
        ahi = c - abig;
        alo = a - ahi;
    }

    template<typename T>
    inline static void twoSum(T a, T b, T &x, T &y) {
        x = (T) (a + b);
        T bvirt = (T) (x - a);
        T avirt = x - bvirt;
        T bround = b - bvirt;
        T around = a - avirt;
        y = around + bround;
    }

    template<typename T>
    inline static void twoOneSum(T a1, T a0, T b, T &x2, T &x1, T &x0) {
        T t0;
        twoSum<T>(a0, b , t0, x0);
        twoSum<T>(a1, t0, x2, x1);
    }

    template<typename T>
    inline static void twoTwoSum(T a1, T a0, T b1, T b0, T &x3, T &x2, T &x1, T &x0) {
        T t0, t1;
        twoOneSum<T>(a1, a0, b0, t0, t1, x0);
        twoOneSum<T>(t0, t1, b1, x3, x2, x1);
    }

    template<typename T>
    inline static void twoDiffTail(T a, T b, T x, T &y) {
        T bvirt = (T) (a - x);
        T avirt = x + bvirt;
        T bround = bvirt - b;
        T around = a - avirt;
        y = around + bround;
    }

    template<typename T>
    inline static void twoDiff(T a, T b, T &x, T &y) {
        x = (T) (a - b);
        twoDiffTail<T>(a, b, x, y);
    }

    template<typename T>
    inline static void twoProduct(T a, T b, T &x, T &y) {
        T ahi, alo, bhi, blo;
        split<T>(a, ahi, alo);
        split<T>(b, bhi, blo);
        x = (T) (a * b);
        T err1 = x - (ahi * bhi);
        T err2 = err1 - (alo * bhi);
        T err3 = err2 - (ahi * blo);
        y = (alo * blo) - err3;
    }

    template<typename T>
    inline static void twoProductPresplit(T a, T b, T bhi, T blo, T &x, T &y) {
        T ahi, alo;
        x = (T) (a * b);
        split<T>(a, ahi, alo);
        T err1 = x - (ahi * bhi);
        T err2 = err1 - (alo * bhi);
        T err3 = err2 - (ahi * blo);
        y = (alo * blo) - err3;
    }

    template<typename T>
    inline static void fastTwoSum(T a, T b, T &x, T &y) {
        x = (T) (a + b);
        T bvirt = x - a;
        y = b - bvirt;
    }

    template<typename T>
    inline static void twoOneDiff(T a1, T a0, T b, T &x2, T &x1, T &x0) {
        T t0;
        twoDiff<T>(a0, b , t0, x0);
        twoSum<T>( a1, t0, x2, x1);
    }

    template<typename T>
    inline static void twoTwoDiff(T a1, T a0, T b1, T b0, T &x3, T &x2, T &x1, T &x0) {
        T t0, t1;
        twoOneDiff<T>(a1, a0, b0, t0, t1, x0);
        twoOneDiff<T>(t0, t1, b1, x3, x2, x1);
    }

    template<typename T>
    inline static void square(T a, T &x, T &y) {
        T ahi, alo;
        x = (T) (a * a);
        split<T>(a, ahi, alo);
        T err1 = x - (ahi * ahi);
        T err3 = err1 - ((ahi + ahi) * alo);
        y = (alo * alo) - err3;
    }

    /**
    * Produce a one-word estimate of an expansion's value
    */
    template<typename T>
    static T estimate(int elen, T* e) {
        T Q;
        int eindex;

        Q = e[0];
        for (eindex = 1; eindex < elen; eindex++) {
            Q += e[eindex];
        }
        return Q;
    }

    /**
    * Sum two expansions, eliminating zero components from the output expansion. 
    */
    template<typename T>
    static int fast_expansion_sum_zeroelim(int elen, T* e, int flen, T* f, T* h) {
        T Q;
        T Qnew;
        T hh;
        int eindex, findex, hindex;
        T enow, fnow;

        enow = e[0];
        fnow = f[0];
        eindex = findex = 0;
        if ((fnow > enow) == (fnow > -enow)) {
            Q = enow;
            enow = e[++eindex];
        }
        else {
            Q = fnow;
            fnow = f[++findex];
        }
        hindex = 0;
        if ((eindex < elen) && (findex < flen)) {
            if ((fnow > enow) == (fnow > -enow)) {
                fastTwoSum<T>(enow, Q, Qnew, hh);
                enow = e[++eindex];
            } else {
                fastTwoSum<T>(fnow, Q, Qnew, hh);
                fnow = f[++findex];
            }
            Q = Qnew;
            if (hh != 0.0) {
                h[hindex++] = hh;
            }
            while ((eindex < elen) && (findex < flen)) {
                if ((fnow > enow) == (fnow > -enow)) {
                    twoSum<T>(Q, enow, Qnew, hh);
                    enow = e[++eindex];
                } else {
                    twoSum<T>(Q, fnow, Qnew, hh);
                    fnow = f[++findex];
                }
                Q = Qnew;
                if (hh != 0.0) {
                    h[hindex++] = hh;
                }
            }
        }
        while (eindex < elen) {
            twoSum<T>(Q, enow, Qnew, hh);
            enow = e[++eindex];
            Q = Qnew;
            if (hh != 0.0) {
                h[hindex++] = hh;
            }
        }
        while (findex < flen) {
            twoSum<T>(Q, fnow, Qnew, hh);
            fnow = f[++findex];
            Q = Qnew;
            if (hh != 0.0) {
                h[hindex++] = hh;
            }
        }
        if ((Q != 0.0) || (hindex == 0)) {
            h[hindex++] = Q;
        }
        return hindex;
    }

    /**
    * Multiply an expansion by a scalar, eliminating zero components from the output expansion.
    */
    template<typename T>
    static int scale_expansion_zeroelim(int elen, T* e, T b, T* h) {
        T Q, sum;
        T hh;
        T product1;
        T product0;
        int eindex, hindex;
        T enow;
        T bhi, blo;

        split<T>(b, bhi, blo);
        twoProductPresplit<T>(e[0], b, bhi, blo, Q, hh);
        hindex = 0;
        if (hh != 0) {
            h[hindex++] = hh;
        }
        for (eindex = 1; eindex < elen; eindex++) {
            enow = e[eindex];
            twoProductPresplit<T>(enow, b, bhi, blo, product1, product0);
            twoSum<T>(Q, product0, sum, hh);
            if (hh != 0) {
                h[hindex++] = hh;
            }
            fastTwoSum<T>(product1, sum, Q, hh);
            if (hh != 0) {
                h[hindex++] = hh;
            }
        }
        if ((Q != 0.0) || (hindex == 0)) {
            h[hindex++] = Q;
        }
        return hindex;
    }

    /**
    * Adaptive exact 2D orientation.
    */
    template<typename T>
    T Robust<T>::orient2dAdapt(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, T detsum) {

        T acx, acy, bcx, bcy;
        T acxtail, acytail, bcxtail, bcytail;
        T detleft, detright;
        T detlefttail, detrighttail;
        T det, errbound;
        T B[4], C1[8], C2[12], D[16];
        T B3;
        int C1length, C2length, Dlength;
        T u[4];
        T u3;
        T s1, t1;
        T s0, t0;

        acx = (T)(a.p - c.p);
        bcx = (T)(b.p - c.p);
        acy = (T)(a.q - c.q);
        bcy = (T)(b.q - c.q);

        twoProduct<T>(acx, bcy, detleft, detlefttail);
        twoProduct<T>(acy, bcx, detright, detrighttail);

        twoTwoDiff(detleft, detlefttail, detright, detrighttail, B3, B[2], B[1], B[0]);
        B[3] = B3;

        det = estimate(4, B);
        errbound = Constants<T>::ccwerrboundB * detsum;
        if ((det >= errbound) || (-det >= errbound)) {
            return det;
        }

        twoDiffTail<T>(a.p, c.p, acx, acxtail);
        twoDiffTail<T>(b.p, c.p, bcx, bcxtail);
        twoDiffTail<T>(a.q, c.q, acy, acytail);
        twoDiffTail<T>(b.q, c.q, bcy, bcytail);

        if ((acxtail == 0.0) && (acytail == 0.0) && (bcxtail == 0.0) && (bcytail == 0.0)) {
            return det;
        }

        errbound = Constants<T>::ccwerrboundC * detsum + Constants<T>::resulterrbound * absolute(det);
        det += (acx * bcytail + bcy * acxtail) - (acy * bcxtail + bcx * acytail);
        if ((det >= errbound) || (-det >= errbound)) {
            return det;
        }

        twoProduct<T>(acxtail, bcy, s1, s0);
        twoProduct<T>(acytail, bcx, t1, t0);
        twoTwoDiff(s1, s0, t1, t0, u3, u[2], u[1], u[0]);
        u[3] = u3;
        C1length = fast_expansion_sum_zeroelim<T>(4, B, 4, u, C1);

        twoProduct<T>(acx, bcytail, s1, s0);
        twoProduct<T>(acy, bcxtail, t1, t0);
        twoTwoDiff(s1, s0, t1, t0, u3, u[2], u[1], u[0]);
        u[3] = u3;
        C2length = fast_expansion_sum_zeroelim<T>(C1length, C1, 4, u, C2);

        twoProduct<T>(acxtail, bcytail, s1, s0);
        twoProduct<T>(acytail, bcxtail, t1, t0);
        twoTwoDiff(s1, s0, t1, t0, u3, u[2], u[1], u[0]);
        u[3] = u3;
        Dlength = fast_expansion_sum_zeroelim<T>(C2length, C2, 4, u, D);

        return(D[Dlength - 1]);
    }

    /**
    * Robust 2D orientation.
    */
    template<typename T>
    T Robust<T>::orient2d(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c) {

        T detleft, detright, det;
        T detsum, errbound;

        detleft = (a.p - c.p) * (b.q - c.q);
        detright = (a.q - c.q) * (b.p - c.p);
        det = detleft - detright;

        if (detleft > 0.0) {
            if (detright <= 0.0) {
                return det;
            } else {
                detsum = detleft + detright;
            }
        }
        else if (detleft < 0.0) {
            if (detright >= 0.0) {
                return det;
            } else {
                detsum = -detleft - detright;
            }
        } else {
            return det;
        }

        errbound = Constants<T>::ccwerrboundA * detsum;
        if ((det >= errbound) || (-det >= errbound)) {
            return det;
        }

        return orient2dAdapt(a, b, c, detsum);
    }
    
    /**
    * Fast non-robust 2D orientation.
    */
    template<typename T>
    T Robust<T>::orient2dFast(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c) {
      T acx, bcx, acy, bcy;
      acx = a.p - c.p;
      bcx = b.p - c.p;
      acy = a.q - c.q;
      bcy = b.q - c.q;
      return acx * bcy - acy * bcx;
    }

    /**
    * Adaptive exact 2D incircle.
    */
    template<typename T>
    T Robust<T>::incircleAdapt(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, const Coordinate<T> &d, T permanent) {
        T adx, bdx, cdx, ady, bdy, cdy;
        T det, errbound;

        T bdxcdy1, cdxbdy1, cdxady1, adxcdy1, adxbdy1, bdxady1;
        T bdxcdy0, cdxbdy0, cdxady0, adxcdy0, adxbdy0, bdxady0;
        T bc[4], ca[4], ab[4];
        T bc3, ca3, ab3;
        T axbc[8], axxbc[16], aybc[8], ayybc[16], adet[32];
        int axbclen, axxbclen, aybclen, ayybclen, alen;
        T bxca[8], bxxca[16], byca[8], byyca[16], bdet[32];
        int bxcalen, bxxcalen, bycalen, byycalen, blen;
        T cxab[8], cxxab[16], cyab[8], cyyab[16], cdet[32];
        int cxablen, cxxablen, cyablen, cyyablen, clen;
        T abdet[64];
        int ablen;

        T fin1[1152];
        T fin2[1152];
        T* finnow, * finother, * finswap;
        int finlength;

        T adxtail, bdxtail, cdxtail, adytail, bdytail, cdytail;
        T adxadx1, adyady1, bdxbdx1, bdybdy1, cdxcdx1, cdycdy1;
        T adxadx0, adyady0, bdxbdx0, bdybdy0, cdxcdx0, cdycdy0;
        T aa[4], bb[4], cc[4];
        T aa3, bb3, cc3;
        T ti1, tj1;
        T ti0, tj0;
        T u[4], v[4];
        T u3, v3;
        T temp8[8], temp16a[16], temp16b[16], temp16c[16];
        T temp32a[32], temp32b[32], temp48[48], temp64[64];
        int temp8len, temp16alen, temp16blen, temp16clen;
        int temp32alen, temp32blen, temp48len, temp64len;
        T axtbb[8], axtcc[8], aytbb[8], aytcc[8];
        int axtbblen, axtcclen, aytbblen, aytcclen;
        T bxtaa[8], bxtcc[8], bytaa[8], bytcc[8];
        int bxtaalen, bxtcclen, bytaalen, bytcclen;
        T cxtaa[8], cxtbb[8], cytaa[8], cytbb[8];
        int cxtaalen, cxtbblen, cytaalen, cytbblen;
        T axtbc[8], aytbc[8], bxtca[8], bytca[8], cxtab[8], cytab[8];
        int axtbclen, aytbclen, bxtcalen, bytcalen, cxtablen, cytablen;
        T axtbct[16], aytbct[16], bxtcat[16], bytcat[16], cxtabt[16], cytabt[16];
        int axtbctlen, aytbctlen, bxtcatlen, bytcatlen, cxtabtlen, cytabtlen;
        T axtbctt[8], aytbctt[8], bxtcatt[8];
        T bytcatt[8], cxtabtt[8], cytabtt[8];
        int axtbcttlen, aytbcttlen, bxtcattlen, bytcattlen, cxtabttlen, cytabttlen;
        T abt[8], bct[8], cat[8];
        int abtlen, bctlen, catlen;
        T abtt[4], bctt[4], catt[4];
        int abttlen, bcttlen, cattlen;
        T abtt3, bctt3, catt3;
        T negate;

        adx = (T)(a.p - d.p);
        bdx = (T)(b.p - d.p);
        cdx = (T)(c.p - d.p);
        ady = (T)(a.q - d.q);
        bdy = (T)(b.q - d.q);
        cdy = (T)(c.q - d.q);

        twoProduct<T>(bdx, cdy, bdxcdy1, bdxcdy0);
        twoProduct<T>(cdx, bdy, cdxbdy1, cdxbdy0);
        twoTwoDiff(bdxcdy1, bdxcdy0, cdxbdy1, cdxbdy0, bc3, bc[2], bc[1], bc[0]);
        bc[3] = bc3;
        axbclen = scale_expansion_zeroelim<T>(4, bc, adx, axbc);
        axxbclen = scale_expansion_zeroelim<T>(axbclen, axbc, adx, axxbc);
        aybclen = scale_expansion_zeroelim<T>(4, bc, ady, aybc);
        ayybclen = scale_expansion_zeroelim<T>(aybclen, aybc, ady, ayybc);
        alen = fast_expansion_sum_zeroelim<T>(axxbclen, axxbc, ayybclen, ayybc, adet);

        twoProduct<T>(cdx, ady, cdxady1, cdxady0);
        twoProduct<T>(adx, cdy, adxcdy1, adxcdy0);
        twoTwoDiff(cdxady1, cdxady0, adxcdy1, adxcdy0, ca3, ca[2], ca[1], ca[0]);
        ca[3] = ca3;
        bxcalen = scale_expansion_zeroelim<T>(4, ca, bdx, bxca);
        bxxcalen = scale_expansion_zeroelim<T>(bxcalen, bxca, bdx, bxxca);
        bycalen = scale_expansion_zeroelim<T>(4, ca, bdy, byca);
        byycalen = scale_expansion_zeroelim<T>(bycalen, byca, bdy, byyca);
        blen = fast_expansion_sum_zeroelim<T>(bxxcalen, bxxca, byycalen, byyca, bdet);

        twoProduct<T>(adx, bdy, adxbdy1, adxbdy0);
        twoProduct<T>(bdx, ady, bdxady1, bdxady0);
        twoTwoDiff(adxbdy1, adxbdy0, bdxady1, bdxady0, ab3, ab[2], ab[1], ab[0]);
        ab[3] = ab3;
        cxablen = scale_expansion_zeroelim<T>(4, ab, cdx, cxab);
        cxxablen = scale_expansion_zeroelim<T>(cxablen, cxab, cdx, cxxab);
        cyablen = scale_expansion_zeroelim<T>(4, ab, cdy, cyab);
        cyyablen = scale_expansion_zeroelim<T>(cyablen, cyab, cdy, cyyab);
        clen = fast_expansion_sum_zeroelim<T>(cxxablen, cxxab, cyyablen, cyyab, cdet);

        ablen = fast_expansion_sum_zeroelim<T>(alen, adet, blen, bdet, abdet);
        finlength = fast_expansion_sum_zeroelim<T>(ablen, abdet, clen, cdet, fin1);

        det = estimate(finlength, fin1);
        errbound = Constants<T>::iccerrboundB * permanent;
        if ((det >= errbound) || (-det >= errbound)) {
            return det;
        }

        twoDiffTail<T>(a.p, d.p, adx, adxtail);
        twoDiffTail<T>(a.q, d.q, ady, adytail);
        twoDiffTail<T>(b.p, d.p, bdx, bdxtail);
        twoDiffTail<T>(b.q, d.q, bdy, bdytail);
        twoDiffTail<T>(c.p, d.p, cdx, cdxtail);
        twoDiffTail<T>(c.q, d.q, cdy, cdytail);
        if ((adxtail == 0.0) && (bdxtail == 0.0) && (cdxtail == 0.0)
            && (adytail == 0.0) && (bdytail == 0.0) && (cdytail == 0.0)) {
            return det;
        }

        errbound = Constants<T>::iccerrboundC * permanent + Constants<T>::resulterrbound * absolute(det);
        det += ((adx * adx + ady * ady) * ((bdx * cdytail + cdy * bdxtail)
            - (bdy * cdxtail + cdx * bdytail))
            + 2.0 * (adx * adxtail + ady * adytail) * (bdx * cdy - bdy * cdx))
            + ((bdx * bdx + bdy * bdy) * ((cdx * adytail + ady * cdxtail)
                - (cdy * adxtail + adx * cdytail))
                + 2.0 * (bdx * bdxtail + bdy * bdytail) * (cdx * ady - cdy * adx))
            + ((cdx * cdx + cdy * cdy) * ((adx * bdytail + bdy * adxtail)
                - (ady * bdxtail + bdx * adytail))
                + 2.0 * (cdx * cdxtail + cdy * cdytail) * (adx * bdy - ady * bdx));
        if ((det >= errbound) || (-det >= errbound)) {
            return det;
        }

        finnow = fin1;
        finother = fin2;

        if ((bdxtail != 0.0) || (bdytail != 0.0)
            || (cdxtail != 0.0) || (cdytail != 0.0)) {
            square<T>(adx, adxadx1, adxadx0);
            square<T>(ady, adyady1, adyady0);
            twoTwoSum<T>(adxadx1, adxadx0, adyady1, adyady0, aa3, aa[2], aa[1], aa[0]);
            aa[3] = aa3;
        }
        if ((cdxtail != 0.0) || (cdytail != 0.0)
            || (adxtail != 0.0) || (adytail != 0.0)) {
            square<T>(bdx, bdxbdx1, bdxbdx0);
            square<T>(bdy, bdybdy1, bdybdy0);
            twoTwoSum<T>(bdxbdx1, bdxbdx0, bdybdy1, bdybdy0, bb3, bb[2], bb[1], bb[0]);
            bb[3] = bb3;
        }
        if ((adxtail != 0.0) || (adytail != 0.0)
            || (bdxtail != 0.0) || (bdytail != 0.0)) {
            square<T>(cdx, cdxcdx1, cdxcdx0);
            square<T>(cdy, cdycdy1, cdycdy0);
            twoTwoSum<T>(cdxcdx1, cdxcdx0, cdycdy1, cdycdy0, cc3, cc[2], cc[1], cc[0]);
            cc[3] = cc3;
        }

        if (adxtail != 0.0) {
            axtbclen = scale_expansion_zeroelim<T>(4, bc, adxtail, axtbc);
            temp16alen = scale_expansion_zeroelim<T>(axtbclen, axtbc, 2.0 * adx, temp16a);

            axtcclen = scale_expansion_zeroelim<T>(4, cc, adxtail, axtcc);
            temp16blen = scale_expansion_zeroelim<T>(axtcclen, axtcc, bdy, temp16b);

            axtbblen = scale_expansion_zeroelim<T>(4, bb, adxtail, axtbb);
            temp16clen = scale_expansion_zeroelim<T>(axtbblen, axtbb, -cdy, temp16c);

            temp32alen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32a);
            temp48len = fast_expansion_sum_zeroelim<T>(temp16clen, temp16c, temp32alen, temp32a, temp48);
            finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
            finswap = finnow; finnow = finother; finother = finswap;
        }
        if (adytail != 0.0) {
            aytbclen = scale_expansion_zeroelim<T>(4, bc, adytail, aytbc);
            temp16alen = scale_expansion_zeroelim<T>(aytbclen, aytbc, 2.0 * ady, temp16a);

            aytbblen = scale_expansion_zeroelim<T>(4, bb, adytail, aytbb);
            temp16blen = scale_expansion_zeroelim<T>(aytbblen, aytbb, cdx, temp16b);

            aytcclen = scale_expansion_zeroelim<T>(4, cc, adytail, aytcc);
            temp16clen = scale_expansion_zeroelim<T>(aytcclen, aytcc, -bdx, temp16c);

            temp32alen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32a);
            temp48len = fast_expansion_sum_zeroelim<T>(temp16clen, temp16c, temp32alen, temp32a, temp48);
            finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
            finswap = finnow; finnow = finother; finother = finswap;
        }
        if (bdxtail != 0.0) {
            bxtcalen = scale_expansion_zeroelim<T>(4, ca, bdxtail, bxtca);
            temp16alen = scale_expansion_zeroelim<T>(bxtcalen, bxtca, 2.0 * bdx, temp16a);

            bxtaalen = scale_expansion_zeroelim<T>(4, aa, bdxtail, bxtaa);
            temp16blen = scale_expansion_zeroelim<T>(bxtaalen, bxtaa, cdy, temp16b);

            bxtcclen = scale_expansion_zeroelim<T>(4, cc, bdxtail, bxtcc);
            temp16clen = scale_expansion_zeroelim<T>(bxtcclen, bxtcc, -ady, temp16c);

            temp32alen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32a);
            temp48len = fast_expansion_sum_zeroelim<T>(temp16clen, temp16c, temp32alen, temp32a, temp48);
            finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
            finswap = finnow; finnow = finother; finother = finswap;
        }
        if (bdytail != 0.0) {
            bytcalen = scale_expansion_zeroelim<T>(4, ca, bdytail, bytca);
            temp16alen = scale_expansion_zeroelim<T>(bytcalen, bytca, 2.0 * bdy, temp16a);

            bytcclen = scale_expansion_zeroelim<T>(4, cc, bdytail, bytcc);
            temp16blen = scale_expansion_zeroelim<T>(bytcclen, bytcc, adx, temp16b);

            bytaalen = scale_expansion_zeroelim<T>(4, aa, bdytail, bytaa);
            temp16clen = scale_expansion_zeroelim<T>(bytaalen, bytaa, -cdx, temp16c);

            temp32alen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32a);
            temp48len = fast_expansion_sum_zeroelim<T>(temp16clen, temp16c, temp32alen, temp32a, temp48);
            finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
            finswap = finnow; finnow = finother; finother = finswap;
        }
        if (cdxtail != 0.0) {
            cxtablen = scale_expansion_zeroelim<T>(4, ab, cdxtail, cxtab);
            temp16alen = scale_expansion_zeroelim<T>(cxtablen, cxtab, 2.0 * cdx, temp16a);

            cxtbblen = scale_expansion_zeroelim<T>(4, bb, cdxtail, cxtbb);
            temp16blen = scale_expansion_zeroelim<T>(cxtbblen, cxtbb, ady, temp16b);

            cxtaalen = scale_expansion_zeroelim<T>(4, aa, cdxtail, cxtaa);
            temp16clen = scale_expansion_zeroelim<T>(cxtaalen, cxtaa, -bdy, temp16c);

            temp32alen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32a);
            temp48len = fast_expansion_sum_zeroelim<T>(temp16clen, temp16c, temp32alen, temp32a, temp48);
            finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
            finswap = finnow; finnow = finother; finother = finswap;
        }
        if (cdytail != 0.0) {
            cytablen = scale_expansion_zeroelim<T>(4, ab, cdytail, cytab);
            temp16alen = scale_expansion_zeroelim<T>(cytablen, cytab, 2.0 * cdy, temp16a);

            cytaalen = scale_expansion_zeroelim<T>(4, aa, cdytail, cytaa);
            temp16blen = scale_expansion_zeroelim<T>(cytaalen, cytaa, bdx, temp16b);

            cytbblen = scale_expansion_zeroelim<T>(4, bb, cdytail, cytbb);
            temp16clen = scale_expansion_zeroelim<T>(cytbblen, cytbb, -adx, temp16c);

            temp32alen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32a);
            temp48len = fast_expansion_sum_zeroelim<T>(temp16clen, temp16c, temp32alen, temp32a, temp48);
            finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
            finswap = finnow; finnow = finother; finother = finswap;
        }

        if ((adxtail != 0.0) || (adytail != 0.0)) {
            if ((bdxtail != 0.0) || (bdytail != 0.0)
                || (cdxtail != 0.0) || (cdytail != 0.0)) {
                twoProduct<T>(bdxtail, cdy, ti1, ti0);
                twoProduct<T>(bdx, cdytail, tj1, tj0);
                twoTwoSum<T>(ti1, ti0, tj1, tj0, u3, u[2], u[1], u[0]);
                u[3] = u3;
                negate = -bdy;
                twoProduct<T>(cdxtail, negate, ti1, ti0);
                negate = -bdytail;
                twoProduct<T>(cdx, negate, tj1, tj0);
                twoTwoSum<T>(ti1, ti0, tj1, tj0, v3, v[2], v[1], v[0]);
                v[3] = v3;
                bctlen = fast_expansion_sum_zeroelim<T>(4, u, 4, v, bct);

                twoProduct<T>(bdxtail, cdytail, ti1, ti0);
                twoProduct<T>(cdxtail, bdytail, tj1, tj0);
                twoTwoDiff(ti1, ti0, tj1, tj0, bctt3, bctt[2], bctt[1], bctt[0]);
                bctt[3] = bctt3;
                bcttlen = 4;
            }
            else {
                bct[0] = 0.0;
                bctlen = 1;
                bctt[0] = 0.0;
                bcttlen = 1;
            }

            if (adxtail != 0.0) {
                temp16alen = scale_expansion_zeroelim<T>(axtbclen, axtbc, adxtail, temp16a);
                axtbctlen = scale_expansion_zeroelim<T>(bctlen, bct, adxtail, axtbct);
                temp32alen = scale_expansion_zeroelim<T>(axtbctlen, axtbct, 2.0 * adx, temp32a);
                temp48len = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp32alen, temp32a, temp48);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
                finswap = finnow; finnow = finother; finother = finswap;
                if (bdytail != 0.0) {
                    temp8len = scale_expansion_zeroelim<T>(4, cc, adxtail, temp8);
                    temp16alen = scale_expansion_zeroelim<T>(temp8len, temp8, bdytail, temp16a);
                    finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp16alen, temp16a, finother);
                    finswap = finnow; finnow = finother; finother = finswap;
                }
                if (cdytail != 0.0) {
                    temp8len = scale_expansion_zeroelim<T>(4, bb, -adxtail, temp8);
                    temp16alen = scale_expansion_zeroelim<T>(temp8len, temp8, cdytail, temp16a);
                    finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp16alen, temp16a, finother);
                    finswap = finnow; finnow = finother; finother = finswap;
                }

                temp32alen = scale_expansion_zeroelim<T>(axtbctlen, axtbct, adxtail, temp32a);
                axtbcttlen = scale_expansion_zeroelim<T>(bcttlen, bctt, adxtail, axtbctt);
                temp16alen = scale_expansion_zeroelim<T>(axtbcttlen, axtbctt, 2.0 * adx, temp16a);
                temp16blen = scale_expansion_zeroelim<T>(axtbcttlen, axtbctt, adxtail, temp16b);
                temp32blen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32b);
                temp64len = fast_expansion_sum_zeroelim<T>(temp32alen, temp32a, temp32blen, temp32b, temp64);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp64len, temp64, finother);
                finswap = finnow; finnow = finother; finother = finswap;
            }
            if (adytail != 0.0) {
                temp16alen = scale_expansion_zeroelim<T>(aytbclen, aytbc, adytail, temp16a);
                aytbctlen = scale_expansion_zeroelim<T>(bctlen, bct, adytail, aytbct);
                temp32alen = scale_expansion_zeroelim<T>(aytbctlen, aytbct, 2.0 * ady, temp32a);
                temp48len = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp32alen, temp32a, temp48);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
                finswap = finnow; finnow = finother; finother = finswap;


                temp32alen = scale_expansion_zeroelim<T>(aytbctlen, aytbct, adytail, temp32a);
                aytbcttlen = scale_expansion_zeroelim<T>(bcttlen, bctt, adytail, aytbctt);
                temp16alen = scale_expansion_zeroelim<T>(aytbcttlen, aytbctt, 2.0 * ady, temp16a);
                temp16blen = scale_expansion_zeroelim<T>(aytbcttlen, aytbctt, adytail, temp16b);
                temp32blen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32b);
                temp64len = fast_expansion_sum_zeroelim<T>(temp32alen, temp32a, temp32blen, temp32b, temp64);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp64len, temp64, finother);
                finswap = finnow; finnow = finother; finother = finswap;
            }
        }
        if ((bdxtail != 0.0) || (bdytail != 0.0)) {
            if ((cdxtail != 0.0) || (cdytail != 0.0)
                || (adxtail != 0.0) || (adytail != 0.0)) {
                twoProduct<T>(cdxtail, ady, ti1, ti0);
                twoProduct<T>(cdx, adytail, tj1, tj0);
                twoTwoSum<T>(ti1, ti0, tj1, tj0, u3, u[2], u[1], u[0]);
                u[3] = u3;
                negate = -cdy;
                twoProduct<T>(adxtail, negate, ti1, ti0);
                negate = -cdytail;
                twoProduct<T>(adx, negate, tj1, tj0);
                twoTwoSum<T>(ti1, ti0, tj1, tj0, v3, v[2], v[1], v[0]);
                v[3] = v3;
                catlen = fast_expansion_sum_zeroelim<T>(4, u, 4, v, cat);

                twoProduct<T>(cdxtail, adytail, ti1, ti0);
                twoProduct<T>(adxtail, cdytail, tj1, tj0);
                twoTwoDiff(ti1, ti0, tj1, tj0, catt3, catt[2], catt[1], catt[0]);
                catt[3] = catt3;
                cattlen = 4;
            }
            else {
                cat[0] = 0.0;
                catlen = 1;
                catt[0] = 0.0;
                cattlen = 1;
            }

            if (bdxtail != 0.0) {
                temp16alen = scale_expansion_zeroelim<T>(bxtcalen, bxtca, bdxtail, temp16a);
                bxtcatlen = scale_expansion_zeroelim<T>(catlen, cat, bdxtail, bxtcat);
                temp32alen = scale_expansion_zeroelim<T>(bxtcatlen, bxtcat, 2.0 * bdx, temp32a);
                temp48len = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp32alen, temp32a, temp48);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
                finswap = finnow; finnow = finother; finother = finswap;
                if (cdytail != 0.0) {
                    temp8len = scale_expansion_zeroelim<T>(4, aa, bdxtail, temp8);
                    temp16alen = scale_expansion_zeroelim<T>(temp8len, temp8, cdytail, temp16a);
                    finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp16alen, temp16a, finother);
                    finswap = finnow; finnow = finother; finother = finswap;
                }
                if (adytail != 0.0) {
                    temp8len = scale_expansion_zeroelim<T>(4, cc, -bdxtail, temp8);
                    temp16alen = scale_expansion_zeroelim<T>(temp8len, temp8, adytail, temp16a);
                    finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp16alen, temp16a, finother);
                    finswap = finnow; finnow = finother; finother = finswap;
                }

                temp32alen = scale_expansion_zeroelim<T>(bxtcatlen, bxtcat, bdxtail, temp32a);
                bxtcattlen = scale_expansion_zeroelim<T>(cattlen, catt, bdxtail, bxtcatt);
                temp16alen = scale_expansion_zeroelim<T>(bxtcattlen, bxtcatt, 2.0 * bdx, temp16a);
                temp16blen = scale_expansion_zeroelim<T>(bxtcattlen, bxtcatt, bdxtail, temp16b);
                temp32blen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32b);
                temp64len = fast_expansion_sum_zeroelim<T>(temp32alen, temp32a, temp32blen, temp32b, temp64);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp64len, temp64, finother);
                finswap = finnow; finnow = finother; finother = finswap;
            }
            if (bdytail != 0.0) {
                temp16alen = scale_expansion_zeroelim<T>(bytcalen, bytca, bdytail, temp16a);
                bytcatlen = scale_expansion_zeroelim<T>(catlen, cat, bdytail, bytcat);
                temp32alen = scale_expansion_zeroelim<T>(bytcatlen, bytcat, 2.0 * bdy, temp32a);
                temp48len = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp32alen, temp32a, temp48);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
                finswap = finnow; finnow = finother; finother = finswap;


                temp32alen = scale_expansion_zeroelim<T>(bytcatlen, bytcat, bdytail, temp32a);
                bytcattlen = scale_expansion_zeroelim<T>(cattlen, catt, bdytail, bytcatt);
                temp16alen = scale_expansion_zeroelim<T>(bytcattlen, bytcatt, 2.0 * bdy, temp16a);
                temp16blen = scale_expansion_zeroelim<T>(bytcattlen, bytcatt, bdytail, temp16b);
                temp32blen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32b);
                temp64len = fast_expansion_sum_zeroelim<T>(temp32alen, temp32a, temp32blen, temp32b, temp64);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp64len, temp64, finother);
                finswap = finnow; finnow = finother; finother = finswap;
            }
        }
        if ((cdxtail != 0.0) || (cdytail != 0.0)) {
            if ((adxtail != 0.0) || (adytail != 0.0)
                || (bdxtail != 0.0) || (bdytail != 0.0)) {
                twoProduct<T>(adxtail, bdy, ti1, ti0);
                twoProduct<T>(adx, bdytail, tj1, tj0);
                twoTwoSum<T>(ti1, ti0, tj1, tj0, u3, u[2], u[1], u[0]);
                u[3] = u3;
                negate = -ady;
                twoProduct<T>(bdxtail, negate, ti1, ti0);
                negate = -adytail;
                twoProduct<T>(bdx, negate, tj1, tj0);
                twoTwoSum<T>(ti1, ti0, tj1, tj0, v3, v[2], v[1], v[0]);
                v[3] = v3;
                abtlen = fast_expansion_sum_zeroelim<T>(4, u, 4, v, abt);

                twoProduct<T>(adxtail, bdytail, ti1, ti0);
                twoProduct<T>(bdxtail, adytail, tj1, tj0);
                twoTwoDiff(ti1, ti0, tj1, tj0, abtt3, abtt[2], abtt[1], abtt[0]);
                abtt[3] = abtt3;
                abttlen = 4;
            }
            else {
                abt[0] = 0.0;
                abtlen = 1;
                abtt[0] = 0.0;
                abttlen = 1;
            }

            if (cdxtail != 0.0) {
                temp16alen = scale_expansion_zeroelim<T>(cxtablen, cxtab, cdxtail, temp16a);
                cxtabtlen = scale_expansion_zeroelim<T>(abtlen, abt, cdxtail, cxtabt);
                temp32alen = scale_expansion_zeroelim<T>(cxtabtlen, cxtabt, 2.0 * cdx, temp32a);
                temp48len = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp32alen, temp32a, temp48);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
                finswap = finnow; finnow = finother; finother = finswap;
                if (adytail != 0.0) {
                    temp8len = scale_expansion_zeroelim<T>(4, bb, cdxtail, temp8);
                    temp16alen = scale_expansion_zeroelim<T>(temp8len, temp8, adytail, temp16a);
                    finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp16alen, temp16a, finother);
                    finswap = finnow; finnow = finother; finother = finswap;
                }
                if (bdytail != 0.0) {
                    temp8len = scale_expansion_zeroelim<T>(4, aa, -cdxtail, temp8);
                    temp16alen = scale_expansion_zeroelim<T>(temp8len, temp8, bdytail, temp16a);
                    finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp16alen, temp16a, finother);
                    finswap = finnow; finnow = finother; finother = finswap;
                }

                temp32alen = scale_expansion_zeroelim<T>(cxtabtlen, cxtabt, cdxtail, temp32a);
                cxtabttlen = scale_expansion_zeroelim<T>(abttlen, abtt, cdxtail, cxtabtt);
                temp16alen = scale_expansion_zeroelim<T>(cxtabttlen, cxtabtt, 2.0 * cdx, temp16a);
                temp16blen = scale_expansion_zeroelim<T>(cxtabttlen, cxtabtt, cdxtail, temp16b);
                temp32blen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32b);
                temp64len = fast_expansion_sum_zeroelim<T>(temp32alen, temp32a, temp32blen, temp32b, temp64);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp64len, temp64, finother);
                finswap = finnow; finnow = finother; finother = finswap;
            }
            if (cdytail != 0.0) {
                temp16alen = scale_expansion_zeroelim<T>(cytablen, cytab, cdytail, temp16a);
                cytabtlen = scale_expansion_zeroelim<T>(abtlen, abt, cdytail, cytabt);
                temp32alen = scale_expansion_zeroelim<T>(cytabtlen, cytabt, 2.0 * cdy, temp32a);
                temp48len = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp32alen, temp32a, temp48);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp48len, temp48, finother);
                finswap = finnow; finnow = finother; finother = finswap;


                temp32alen = scale_expansion_zeroelim<T>(cytabtlen, cytabt, cdytail, temp32a);
                cytabttlen = scale_expansion_zeroelim<T>(abttlen, abtt, cdytail, cytabtt);
                temp16alen = scale_expansion_zeroelim<T>(cytabttlen, cytabtt, 2.0 * cdy, temp16a);
                temp16blen = scale_expansion_zeroelim<T>(cytabttlen, cytabtt, cdytail, temp16b);
                temp32blen = fast_expansion_sum_zeroelim<T>(temp16alen, temp16a, temp16blen, temp16b, temp32b);
                temp64len = fast_expansion_sum_zeroelim<T>(temp32alen, temp32a, temp32blen, temp32b, temp64);
                finlength = fast_expansion_sum_zeroelim<T>(finlength, finnow, temp64len, temp64, finother);
                finswap = finnow; finnow = finother; finother = finswap;
            }
        }

        T ret = finnow[finlength - 1];
        return ret;
    }

    /**
    * Robust 2D incircle.
    */
    template<typename T>
    T Robust<T>::incircle(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, const Coordinate<T> &d) {
        T adx, bdx, cdx, ady, bdy, cdy;
        T bdxcdy, cdxbdy, cdxady, adxcdy, adxbdy, bdxady;
        T alift, blift, clift;
        T det;
        T permanent, errbound;

        adx = a.p - d.p;
        bdx = b.p - d.p;
        cdx = c.p - d.p;
        ady = a.q - d.q;
        bdy = b.q - d.q;
        cdy = c.q - d.q;

        bdxcdy = bdx * cdy;
        cdxbdy = cdx * bdy;
        alift = adx * adx + ady * ady;

        cdxady = cdx * ady;
        adxcdy = adx * cdy;
        blift = bdx * bdx + bdy * bdy;

        adxbdy = adx * bdy;
        bdxady = bdx * ady;
        clift = cdx * cdx + cdy * cdy;

        det = alift * (bdxcdy - cdxbdy)
            + blift * (cdxady - adxcdy)
            + clift * (adxbdy - bdxady);

        permanent = (absolute(bdxcdy) + absolute(cdxbdy)) * alift
            + (absolute(cdxady) + absolute(adxcdy)) * blift
            + (absolute(adxbdy) + absolute(bdxady)) * clift;
        errbound = Constants<T>::iccerrboundA * permanent;
        if ((det > errbound) || (-det > errbound)) {
            return det;
        }

        return incircleAdapt(a, b, c, d, permanent);
    }
    
    /**
    * Fast non-robust 2D incircle.
    */
    template<typename T>
    T Robust<T>::incircleFast(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, const Coordinate<T> &d) {
      T adx, ady, bdx, bdy, cdx, cdy;
      T abdet, bcdet, cadet;
      T alift, blift, clift;

      adx = a.p - d.p;
      ady = a.q - d.q;
      bdx = b.p - d.p;
      bdy = b.q - d.q;
      cdx = c.p - d.p;
      cdy = c.q - d.q;

      abdet = adx * bdy - bdx * ady;
      bcdet = bdx * cdy - cdx * bdy;
      cadet = cdx * ady - adx * cdy;
      alift = adx * adx + ady * ady;
      blift = bdx * bdx + bdy * bdy;
      clift = cdx * cdx + cdy * cdy;

      return alift * bcdet + blift * cadet + clift * abdet;
    }

    // Float type definitions
    template float Robust<float>::orient2d(const Coordinate<float> &a, const Coordinate<float> &b, const Coordinate<float> &c);
    template float Robust<float>::orient2dFast(const Coordinate<float> &a, const Coordinate<float> &b, const Coordinate<float> &c);
    template float Robust<float>::incircle(const Coordinate<float> &a, const Coordinate<float> &b, const Coordinate<float> &c, const Coordinate<float> &d);
    template float Robust<float>::incircleFast(const Coordinate<float> &a, const Coordinate<float> &b, const Coordinate<float> &c, const Coordinate<float> &d);
        
    // Double type definitions
    template double Robust<double>::orient2d(const Coordinate<double> &a, const Coordinate<double> &b, const Coordinate<double> &c);
    template double Robust<double>::orient2dFast(const Coordinate<double> &a, const Coordinate<double> &b, const Coordinate<double> &c);
    template double Robust<double>::incircle(const Coordinate<double> &a, const Coordinate<double> &b, const Coordinate<double> &c, const Coordinate<double> &d);
    template double Robust<double>::incircleFast(const Coordinate<double> &a, const Coordinate<double> &b, const Coordinate<double> &c, const Coordinate<double> &d);
}