/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_PNG_H
#define GEOSTACK_PNG_H

#include <string>

#include "gs_raster.h"

namespace Geostack
{
    template <typename RTYPE, typename CTYPE>
    class PngHandler : public RasterFileHandler<RTYPE, CTYPE> {

    public:

        /**
        * %PngHandler constructor.
        */
        PngHandler(): RasterFileHandler<RTYPE, CTYPE>() { }
        
        void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Open file for reading to %Raster
        void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Write %Raster to file
    };
}

#endif
