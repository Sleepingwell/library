/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_DELAUNAY_H
#define GEOSTACK_DELAUNAY_H

#include <vector>
#include <list>
#include <map>

#include "gs_vector.h"

namespace Geostack {

    template<typename T>
    class Delaunay {

    public:

        static Vector<T> triangulate(CoordinateList<T> coords, const std::vector<uint64_t> edgeIndexes = std::vector<uint64_t>(),
            std::size_t geometryType = GeometryType::Polygon);
    };


}
#endif
