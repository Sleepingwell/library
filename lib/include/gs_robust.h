/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "gs_vector.h"

namespace Geostack {

    template <typename T>
    class Robust {

    public:
    
        static T orient2d(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c);
        static T orient2dFast(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c);

        static T incircle(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, const Coordinate<T> &d);
        static T incircleFast(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, const Coordinate<T> &d);

    private:

        static T orient2dAdapt(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, T detsum);

        static T incircleAdapt(const Coordinate<T> &a, const Coordinate<T> &b, const Coordinate<T> &c, const Coordinate<T> &d, T permanent);
    };

}